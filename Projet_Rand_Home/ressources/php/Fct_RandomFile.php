<?php
	//compte les fichier d'une certaine extension
	function countfiles($directory, $extention)
	{
		$files = glob($directory . $extention);
		if ( $files !== false )
		{
		    $filecount = count( $files );
		    return $filecount;
		}
		else
		{
		    return 0;
		}
	}
?>

<?php
	function randomfile($directory, $extention)
	{
		$cpt = 0;
		$numfile = rand(1, countfiles($directory, '*'.$extention));
		if (is_dir($directory))
		{
		   if ($dh = opendir($directory))
		   {
		       while ( ($file = readdir($dh)) !== false && $cpt < $numfile)
		       {
		           if( $file != '.' && $file != '..' && strstr($file, $extention)) 
		           {
		           		$cpt++;
		           		$temp = $file;
		           }
		       }
		       closedir($dh);
		   }
		}
		return $temp;
	}
?>