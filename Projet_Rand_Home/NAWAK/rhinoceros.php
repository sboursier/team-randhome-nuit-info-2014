<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Grande Reflexion...</title>
    </head>
    <body>

        <style>
            #form {
                height: 600px;
                width: 100%;
                margin: auto;
            }
            /*            #form > input {
                            width: 50%;
                            height: 100%;
                            float: left;
            
                        }*/
            input[type=button]:hover {
                box-shadow: -1px 2px 10px 3px #960505;
            }
            #form > input{
                width: 50%;
                height: 200px;
                border-radius : 10px;
                background-color: #1F0006;
                text-decoration: none;
                font-weight: bold;
                color: white;
                font-size: 50px;
                margin: auto;
            }

            .article {
                width: 90%;
                border-radius : 10px;
                background-color: white;
                margin-left: 5%;
                box-shadow : 0px 0px 2.5px #960505;
                margin-bottom: 20px;
                position: relative;
                float : left;
            }
            .art_content {
                width: 90%;
                margin-left: 5%;
                padding-top: 20px;
                padding-bottom: 20px;
                text-align: center;
                font-weight: bold;
                font-size: 50px;

            }
        </style>

        <section class="article">
            <p class="art_content">Pensez-vous que l'on peut dire d'un Rhinocéros dans le brouillard qu'il a une corne de brume?</p>
        </section>
        <form id="form">  
            <input type="button" value="Oui" onclick="Play('Ow_Yeah.mp3')"/><input type="button" value="Non" onmouseover="Move()"/>
        </form>

        <section id="play"></section>

        <script type="text/javascript" src="/ressources/js/Fct_Play.js"></script>
        <script type="text/javascript" src="/ressources/js/Fct_Move.js"></script>
    </body>
</html>

